<?php

/**
 * @file
 * Contains \Drupal\field_expression\Plugin\Field\FieldType\ExpressionField.
 */

namespace Drupal\field_expression\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'expression_field' field type.
 *
 * @FieldType(
 *   id = "expression_field",
 *   label = @Translation("Expression"),
 *   description = @Translation("The value of this field is created by evaluating the configured php expression."),
 *   default_formatter = "expression_formatter"
 * )
 */
class ExpressionField extends FieldItemBase {
  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'expression_code' => '$value = \'\';',
      'formatter_code' => '$formatted = $value;',
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'data_type' => 'varchar',
      'data_length' => null,
      'default_value' => '-',
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // Prevent early t() calls by using the TranslatableMarkup.
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Text value'))
      ->setRequired(TRUE);
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = [
      'columns' => [
        'value' => [
          'type' => 'varchar',
          'length' => (int) $field_definition->getSetting('data_length'),
          'binary' => FALSE,
        ],
      ],
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = [];

    $element['expression_code'] = [
      '#type' => 'textarea',
      '#title' => $this->t('PHP expression to evaluate'),
      '#default_value' => $this->getSetting('expression_code'),
      '#required' => FALSE,
      '#description' => $this->t('<p>This code should assign a value to the <b><code>$value</code></b> variable. The variables also available to your code include: <code>$fields</code>, <code>$entity</code>, <code>$entity_manager</code>. For multi-value <em>expression</em> fields <code>$value</code> should be an array. Here\'s a simple example which sets the <em>expression</em> field\'s value to the value of the sum of the number fields (<code>field_a</code> and <code>field_b</code>) in an entity:</p><p><code>$value = $entity->field_a->value + $entity->field_b->value;</code></p>')
    ];

    $element['formatter_code'] = [
      '#type' => 'textarea',
      '#title' => $this->t('PHP expression to format the $value'),
      '#default_value' => $this->getSetting('formatter_code'),
      '#required' => FALSE,
      '#description' => $this->t('This code should assign a string to the <b><code>$formatted</code></b> variable, which will be printed when the formatted output is selected. The result of the <em>expression</em> field is in <code>$value</code>. Also following variables are available: <code>$fields</code>, <code>$entity</code>, <code>$entity_manager</code>. Note: In the formatter you can decide whether to output the content of $formatter (\'formatted\') or $value (\'unformatted\').')
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $elements = [];

    $elements['data_length'] = [
      '#type' => 'number',
      '#title' => $this->t('Data length'),
      '#default_value' => $this->getSetting('data_length'),
      '#required' => TRUE,
      '#description' => $this->t('The maximum length of the expression result in characters.'),
      '#min' => 1,
      '#disabled' => $has_data,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    $value = $this->getEvaluatedValue();
    $this->setValue($value);
  }

  /**
   * Executes the code in expression_code and returns the resulting string
   *
   * @return string
   *   the resulting string
   */
  public function getEvaluatedValue() {
    $code = $this->getSetting('expression_code');
    $entity_manager = \Drupal::EntityManager();
    $entity = $this->getEntity();
    $fields = $this->getEntity()->toArray();
    $value = NULL;

    eval($code);
    return $value;
  }

  /**
   * Executes the formatter code on the saved value or the recalculated value
   *
   * @param boolen
   *   A indicator, where to take the value from
   *   TRUE use the saved value
   *   FALSE recalculate the value and then apply the formatter code
   *
   * @return string
   *   the resulting string
   */
  public function getFormattedValue($saved = TRUE) {
    $code = $this->getSetting('formatter_code');
    $entity_manager = \Drupal::EntityManager();
    $entity = $this->getEntity();
    $fields = $this->getEntity()->toArray();

    $value = $saved ? $this->get('value')->getValue() : $this->getEvaluatedValue();
    $formatted = NULL;

    eval($code);
    return $formatted;
  }
}

