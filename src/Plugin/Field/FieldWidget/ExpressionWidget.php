<?php

/**
 * @file
 * Contains \Drupal\field_expression\Plugin\Field\FieldWidget\ExpressionWidgetType.
 */

namespace Drupal\field_expression\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'expression_widget' widget.
 *
 * @FieldWidget(
 *   id = "expression_widget",
 *   label = @Translation("Expression (temporary!)"),
 *   field_types = {
 *     "expression_field"
 *   }
 * )
 */
class ExpressionWidget extends WidgetBase {
  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('This field should only be shown if it was added to the field list after entities have been created. You can then save all these entities to apply the default value and then hide this field from the edit form.');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = [];

    $element['value'] = $element + [
        '#placeholder' => $this->fieldDefinition->getName(),
        '#type' => 'textfield',
        '#default_value' => '',
        '#disabled' => TRUE,
        '#description'=> $this->t('This field should only be shown if it was added to the field list after entities have been created. You can then save all these entities to apply the default value and then hide this field from the edit form.'),
      ];

    return $element;
  }

}
