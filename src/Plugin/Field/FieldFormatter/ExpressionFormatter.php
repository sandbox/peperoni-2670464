<?php

/**
 * @file
 * Contains \Drupal\field_expression\Plugin\Field\FieldFormatter\ExpressionFormatter.
 */

namespace Drupal\field_expression\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\Validator\Constraints\False;

/**
 * Plugin implementation of the 'expression_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "expression_formatter",
 *   label = @Translation("Expression"),
 *   field_types = {
 *     "expression_field"
 *   }
 * )
 */
class ExpressionFormatter extends FormatterBase {

  private function formatterOptions() {
    return [
      0 => t('Unformatted (saved)'),
      1 => t('Unformatted (recalculated)'),
      2 => t('Formatted (saved value)'),
      3 => t('Formatted (recalculated value)')
    ];
  }

  private function quantityOptions() {
    return [
      1 => t('seconds'),
      60 => t('minutes'),
      3600 => t('hours'),
      84400 => t('days'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
      'sanitized' => TRUE,
      'formatter' => 2,
      'cache' => -1,
      'cache_units' => 1,
    ) + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      'formatter' => [
        '#type' => 'select',
        '#title' => t('Formatter'),
        '#default_value' => $this->getSetting('formatter'),
        '#options' => $this->formatterOptions(),
      ],
      'sanitized' => [
        '#type' => 'checkbox',
        '#title' => t('Sanitized'),
        '#default_value' => $this->getSetting('sanitized'),
      ],
      'cache' => [
        '#type' => 'number',
        '#title' => t('Cache life time'),
        '#min' => -1,
        '#default_value' => $this->getSetting('cache'),
        '#attached' => [
          'library' => 'field_expression/css/field_expression_formatter_ui.css'
        ],
      ],
      'cache_units' => [
        '#type' => 'select',
        '#default_value' => $this->getSetting('cache_units'),
        '#options' => $this->quantityOptions(),
        '#description' => t('Enter -1 (permanent, i.e. until saved again) or number of units to cache the value. If you use the formatted or unformatted saved value, you should normally leave the value to -1.'),
        '#class' => 'inline',
      ],
    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $cache = $this->getSetting('cache');
    $cache_units = $this->getSetting('cache_units');
    $summary = [];
    $summary[] = $this->getSetting('sanitized') ? t('Sanitized') : t('Unsanitized');
    $summary[] = $this->formatterOptions()[$this->getSetting('formatter')];
    if ($this->getSetting('cache') < 0) {
      $summary[] = t('Cache lifetime: permanent', $options = ['context' => 'expression_formatter']);
    }
    elseif ($this->getSetting('cache_units') == 1) { // seconds
      $summary[] = t('Cache lifetime: @lifetime seconds',
        $args = ['@lifetime' => $cache],
        $options = ['context' => 'expression_formatter']
      );
    }
    else {
      $summary[] = t('Cache lifetime: @lifetime @quantity (@seconds seconds)',
        $args = [
          '@lifetime' => $cache,
          '@quantity' => $this->quantityOptions()[$cache_units],
          '@seconds' => $cache * $cache_units,
        ],
        $options = ['context' => 'expression_formatter']
      );
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $cache = ($this->getSetting('cache') >= 0) ? $this->getSetting('cache') : -1;
    $cache_units = ($cache == -1) ? 1 : $this->getSetting('cache_units');
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#markup' => $this->viewValue($item),
        '#cache' => [
          'keys' => [
            $items->getEntity()->getEntityTypeId(),
            $items->getEntity()->bundle(),
            $this->viewMode,
          ],
          'max-age' => $cache * $cache_units,
        ]
      ];
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    $value = '';
    switch ($this->getSetting('formatter')) {
      case 0: // Unformatted (saved)
        $value = $item->get('value')->getValue();
        break;
      case 1: // Unformatted (recalculated)
        $value = $item->getEvaluatedValue();
        break;
      case 2: //Formatted (saved value)
        $value = $item->getFormattedValue();
        break;
      case 3: // Formatted (recalculated value)
        $value = $item->getFormattedValue(FALSE);
    }
    if ($this->getSetting('sanitized')) {
      return nl2br(Html::escape($value));
    }
    else {
      return nl2br($value);
    }
  }

}
