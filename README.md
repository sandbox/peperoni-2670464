# field: expression
## Description
This field provides a widget to enter php-code to compute the value. This value can be computed from other values of the same entity or from system values. The value is always stored in the database but you can decide whether to use the stored value or recompute the value before displaying it. It also contains a formatter to format the computed value.

## Usage
* Download and install the module as usual. No additional configuration is needed.
* Add a field of type "Expression" to your entity bundle.
* Specify the expected maximum length of the value as it is stored in the database.
* Enter the php-expression to compute the value. Hints are contained in the help-text below the value field.
* Enter the php-expression to format the value for output. Hints are contained in the help-text below the field.
* Go to the **Manage form display** tab. If the entity bundle is completely new, i.e. it has no content, you can move the new field to the *Disabled* area, otherwise leave it above this area. This is due to the fact, that Drupal does not use default values on fields of existing content if the field is disabled.
* Go to the **Manage display** tab. Select *Expression* as a formatter of that field and configure the formatter.
  * You can choose whether to use saved or recalculated values for display. Also you can choose to use the value itself or the formatted value for display. The recalculated value is newly computed, if the cache is rebuilt or the cached value is out-dated (see *Cache life time* below).
  * by default the value is sanitized before display, so all html is escaped. If you have html in the output, you should uncheck this field.
  * Drupal is caching all values as much as possible. But here you can determine, how long the cached value can exist before it is rebuilt. If your expression only consists of values from the same node, leave it on permanent. The value is reevaluated on each save of the node. But if you use variables like dates or times to build the value, set a cache lifetime according to the change interval of the variables.
  * The cache lifetime can be expressed in seconds, minutes, hours or days.
  * update the configuration and don't forget to save the settings!
   
If there is already some content for this entity, Drupal ignores this field unless it is displayed on the edit form. Therefore the field is initially added to the displayable area of the form. Re-save all existing content and, when done, move the field to the disabled area in the edit form.

## Credits
This module was inspired by [computed_field]. But computed field (at time of this writing) didn't work correctly. It has different datatypes for the value (Varchar, text, integer, decimal, float) with mixed additional fields (length, decimals and so on) which were confusing. And the number fields did not work. So I decided to use varchar only (you can easily transform it into any number field, but be aware of sorting!). Also I added the choice of using the value or the formatted value for display. Last but not least I added the cache lifetime in case Drupal's caching is too aggressive.

Enjoy!

[computed_field]: https://www.drupal.org/project/computed_field